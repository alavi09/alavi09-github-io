/**
 * Function which given an html file, returns the text inside the first h1 tag.
 * by alavi09
 */

const path = require("path");
const fs = require("fs");
const Maybe = require("data.Maybe");

// Function that takes in a markdown file, 'fileloc' and returns the title based 
// on the contents of first h1 tag.
export default function getTitle(fileloc) {

    // get the title of the writing (first h1 tag)
    const getWritingTitle = function (writing) {
        try {
            const heading = writing.match(/^<h1(.+)<\/h1>/)[0],
                headingText = heading.replace(/<h1(.*)">|<\/h1>/gi, "");
            return Maybe.Just(headingText).value;
        } catch(e){
            return Maybe.Nothing()
        }
    }

    // store document content
    //const data = fs.readFileSync(fileloc, "utf-8");
    try {
        const data = fs.readFileSync(fileloc, "utf-8");
        // get title from file and convert to lowercase, replace spaces with -
        const title = getWritingTitle(data).replace(/\s+/g, '-').toLowerCase();

        return Maybe.Just(title).value;
    } catch (e) {
        return Maybe.Just(e)
    }


}

//console.log(getTitle('./test.html'))