import gulp from 'gulp'
import markdown from 'gulp-markdown'
import flatmap from 'gulp-flatmap'
import rename from 'gulp-rename'
import path from 'path'
import getTitle from './utils/getTitle'
import concat from 'gulp-concat'
import replace from 'gulp-replace'
import del from 'del'
import vp from 'vinyl-paths'

// store directory locations for files
const dirs = {
    src: {
        markdown: 'markdown/', // location of markdown files to be converted
        htmlArticles: 'tempArticles/', // temp location for conversions
        htmlViews: 'html_views/' // html views(header, footer)
    },
    dest: {
        markdown: 'tempArticles/', // destination of markdown files being converted
        processedMd: 'processed_md', // copies of transformed markdown files
        html: 'assets/articles/' // final destination of html files
    }
}

// function to convert markdown to html. using flatmap to process each file.
function md() {
    return gulp.src(dirs.src.markdown + '*.md')
        .pipe(flatmap(function (stream, file) {
            return gulp.src(path.resolve(file.path))
                //
                .pipe(vp(del))
                .pipe(gulp.dest(dirs.dest.processedMd))
                .pipe(markdown())
                .pipe(rename( path.basename(file.path, '.md' )  + '.html'))
        }))
        .pipe(gulp.dest(dirs.dest.markdown));

}

// concat final html and use heading as filename 
function html() {
    return gulp.src(dirs.src.htmlArticles +'*.html')
        .pipe(flatmap(function (stream, file) {
            return gulp.src([dirs.src.htmlViews+'header.html', path.resolve(file.path), dirs.src.htmlViews+'footer.html'])
                .pipe(concat('article.html'))
                .pipe(replace('<h1 ', '<h1 class="f2-large f-subheadline-l lh-title fw9 near-white pa4 pt5" '))
                .pipe(replace('</h1>', '</h1><h2 class="f4 fw6 near-white i pa4 pb1">By Ali Alavi</h2>'))
                .pipe(replace('</h2>', '</h2></div><div class="serif center w-90">'))
                .pipe(replace('<p>', '<p class="pv4-ns f4-ns">'))
                .pipe(replace('<pre>', '<pre class="prettyprint pre">'))
                .pipe(replace('</article>', '</div></article>'))
                .pipe(rename(getTitle(path.resolve(file.path))+ '.html'))//getTitle(path.resolve(file.path))
        }))

        .pipe(gulp.dest(dirs.dest.html))
}

// delete temp folder
function clean() {
    return del([dirs.dest.markdown]);
}

// sync process tasks
gulp.task('default', gulp.series(md, html, clean));

// watch
function watch() {
  gulp.watch(`${dirs.src.markdown}*.md`, gulp.series(md, html, clean));
}

gulp.task('watch', watch);

//for node debug only
gulp.task('task', gulp.series(watch));